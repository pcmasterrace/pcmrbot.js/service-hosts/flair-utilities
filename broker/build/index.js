"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const moleculer_1 = require("moleculer");
const winston = require("winston");
const moment = require("moment");
const { extend } = moleculer_1.Logger;
const core_accounts_1 = require("@pcmrbotjs/core-accounts");
const core_reddit_1 = require("@pcmrbotjs/core-reddit");
const tool_flair_utilities_1 = require("@pcmrbotjs/tool-flair-utilities");
const broker = new moleculer_1.ServiceBroker({
    logFormatter: "short",
    transporter: "TCP",
    namespace: "pcmrbotjsv2",
    logger: bindings => extend(winston.createLogger({
        format: winston.format.combine(winston.format.label({ label: bindings.mod }), winston.format.timestamp(), winston.format.colorize(), winston.format.printf(({ level, message, label, timestamp }) => {
            return `[${moment(timestamp).format("HH:mm:ss.SSS")}] ${level} ${label.toUpperCase()}: ${message}`;
        })),
        transports: [
            new winston.transports.Console(),
        ]
    }))
});
core_accounts_1.default(broker);
core_reddit_1.default(broker);
tool_flair_utilities_1.default(broker);
broker.start();
