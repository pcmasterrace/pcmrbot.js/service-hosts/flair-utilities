const { ServiceBroker } = require("moleculer");

const broker = new ServiceBroker({
	logFormatter: "simple",
	transporter: "TCP",
	namespace: "pcmrbotjsv2"
});

broker.createService({
	name: "test",
	actions: {
		stream: {
			name: "stream",
			params: {
				subreddit: "string"
			},
			async handler(ctx) {
				let stream = await this.broker.call("v2.reddit.flair.get.all", {
					subreddit: ctx.params.subreddit
				});

				stream.on("data", (chunk) => this.logger.info(`Received chunk: ${chunk}`));
			}
		}
	}
})

broker.repl();
broker.start();