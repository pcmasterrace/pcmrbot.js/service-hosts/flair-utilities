import { ServiceBroker, Logger } from "moleculer";
import * as winston from "winston";
import * as SlackHook from "winston-slack-webhook-transport";
import * as moment from "moment";
const {extend} = Logger;

import registerAllAccountServices from "@pcmrbotjs/core-accounts";
import registerAllRedditServices from "@pcmrbotjs/core-reddit";
import registerAllFlairUtilities from "@pcmrbotjs/tool-flair-utilities";

const broker = new ServiceBroker({
    logFormatter: "short",
    transporter: "TCP",
    namespace: "pcmrbotjsv2",
    logger: bindings => extend(winston.createLogger({
        format: winston.format.combine(
            winston.format.label({ label: bindings.mod }),
            winston.format.timestamp(),
            winston.format.colorize(),
            winston.format.printf(({ level, message, label, timestamp }) => {
                return `[${moment(timestamp).format("HH:mm:ss.SSS")}] ${level} ${label.toUpperCase()}: ${message}`;
            })
        ),
        transports: [
            new winston.transports.Console(),
            /*
            new SlackHook({
                webhookUrl: process.env.LOGGER_WEBHOOK_URL,
                mrkdwn: true,
                formatter: info => {
                    return {
                        // TODO: The square brackets are a little borked on Slack, not sure why. Investigate
                        text: `[${info.level}] \`${info.label.toUpperCase()}\`: ${info.message}`
                    }
                }
            })
            */
        ]
    }))
});

registerAllAccountServices(broker);
registerAllRedditServices(broker);
registerAllFlairUtilities(broker);

broker.start()